/*	File primary.c: 2.4 (84/11/27,16:26:07) */
/*% cc -O -c %
 *
 */

#ifndef _PRIMARY_H
#define _PRIMARY_H

int primary (int* lval);
int dbltest (int val1[],int val2[]);
void result (int lval[],int lval2[]);
int constant (int val[]);
int number (int val[]);
int pstr (int val[]);
int qstr (int val[]);
int readqstr (void );
int readstr (void );
int spechar(void );

#endif

