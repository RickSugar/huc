/*	File expr.c: 2.2 (83/06/21,11:24:26) */
/*% cc -O -c %
 *
 */

#ifndef _EXPR_H
#define _EXPR_H

void expression (int comma);
int heir1 (int * lval);
int heir1a (int *lval);
int heir1b (int *lval);
int heir1c (int *lval);
int heir2 (int *lval);
int heir3 (int *lval);
int heir4 (int *lval);
int heir5 (int *lval);
int heir6 (int *lval);
int heir7 (int *lval);
int heir8 (int *lval);
int heir9 (int *lval);
int heir10 (int *lval);
int heir11 (int *lval);
void store (int *lval);
void rvalue (int *lval);
void needlval (void );

#endif

